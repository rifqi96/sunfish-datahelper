<?php

class Users extends DB\SQL\Mapper{

    public function __construct(DB\SQL $db){
        parent::__construct($db, 'MsUser');
    }

    public function all(){
        $this->load();
        return $this->query;
    }

    public function getById($id){
        $this->load(array('id=?', $id));
        return $this->query;
    }

    public function getByUsername($username){
        $this->load(array('username=?', $username));
        return $this->query;
    }

    public function getByEmail($email){
        $this->load(array('email=?', $email));
        return $this->query;
    }

    public function getByLevel($user_level){
        $this->load(array('user_level=?', $user_level));
        return $this->query;
    }

    public function getBy($params){
        $this->load($params);
        return $this->query;
    }

    public function add(){
        $this->copyFrom("POST");
        $this->save();
    }

    public function edit($param){
        $this->load($param);
        $this->copyFrom("POST");
        $this->update();
    }

    public function delete($id){
        $this->load(array('id=?',$id));
        $this->erase();
    }
}