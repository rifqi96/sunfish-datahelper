<?php

class Modules{
    public $name;
    public $hasUrl;
    public $url;
    public $active;

    public function expose() {
        return get_object_vars($this);
    }

    public function __construct($name, $hasUrl, $url, $active)
    {
        $this->name = $name;
        $this->hasUrl = $hasUrl;
        $this->url = $url;
        $this->active = $active;
    }
}