<?php

class ReportController extends AppController{

    public function beforeroute()
    {
        parent::beforeroute(); // TODO: Change the autogenerated stub
        if($this->f3->get('SESSION.user')===null){
            $this->f3->reroute('/login');
            exit;
        }
    }

    public function home($f3){
        $tasks = new Tasks($this->db);
        $task = array();
        $task['all'] = $tasks->all();
        $task['process'] = $tasks->all(array('status=?', 'process'));
        $task['completed'] = $tasks->all(array('status=?', 'completed'));
        $task['pending'] = $tasks->all(array('status=?', 'pending'));
        $task['draft'] = $tasks->all(array('status=?', 'draft'));

        $f3->set('task', $task);
        $f3->set('content', 'report.html');
        $f3->set('header_title', 'Report');
        $f3->set('subtitle', 'Report');
        echo $this->template->render('layout.html');
    }
}