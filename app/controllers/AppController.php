<?php

class AppController{
	protected $f3;
	protected $db;
	protected $server_name;
	protected $current_url;
	protected $audit;

    public function beforeroute(){

	}

    public function afterroute(){

	}
	
	public function __construct(){
		$f3 = Base::Instance();
		$this->f3 = $f3;
		$db = new DB\SQL(
			$f3->get('devdb'),
			$f3->get('devdbusername'),
			$f3->get('devdbpassword'),
			array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
		);
		$this->db = $db;
        $this->template = new Template;
        $this->current_url = $this->f3->get('PATH');
        $this->server_name = $_SERVER['SERVER_NAME'];
        $this->f3->set('current_url', $this->current_url);
        $this->f3->set('server_name', $this->server_name);
        $audit = \Audit::instance();
        $this->audit = $audit;
        $base = $this->f3->get('BASE');
        $this->f3->set('base', $base);
        $this->f3->set('TZ', 'Asia/Jakarta');
        $this->f3->set('breadcrumb', 'breadcrumb.html');
        $this->f3->set('modules', $this->createModules());
        $this->f3->set('headersection', 'headersection.html');
        $this->redirect();
    }

    public function createModules()
    {
        $module_names = explode('/', $this->current_url);
        array_shift($module_names);
        $modules = array();
        $urls = array('');
        $url = "";
        array_push($modules, new Modules('Overview', true, $urls[0], true));
        foreach ($module_names as $key => $val) {
            $name = $val;
            $active = false;
            $hasUrl = true;
            array_push($urls, $val);
            $url = $url . $urls[$key + 1] . "/";
            if ($key == count($module_names) - 1) {
                $active = true;
                $hasUrl = false;
                $url = "";
            }
            array_push($modules, new Modules(ucwords($name), $hasUrl, $url, $active));
        }
        $modulesJSON = Array();
        foreach ($modules as $key => $value) {
            array_push($modulesJSON, $modules[$key]->expose());
        }
        $this->f3->set('module_names', $module_names);
        return $modulesJSON;
    }

    public function redirect()
    {
        if ($this->current_url === '/task/view' || $this->current_url === '/task/edit') {
            $this->f3->reroute('/task');
        }
    }
}