<?php
class UserController extends AppController{

    public function login($f3){
        if($f3->get('SESSION.user')!==null){
            $f3->reroute('/');
        }
        echo $this->template->render('login.html');
        $f3->set("SESSION.loginerror", "");
        $f3->clear('SESSION');
    }

    public function authenticate($f3){
        $username = $f3->get('POST.username');
        $password = $f3->get('POST.password');

        $users = new Users($this->db);
        $users->getByUsername($username);

        if($users->dry()){
            $this->f3->set("SESSION.loginerror", "User does not exist");
            $this->f3->reroute('/login');
        }

        if(password_verify($password, $users->password)){
            $this->f3->set('SESSION.user', $users->username);
            $this->f3->set('SESSION.role', $users->user_level);
            $this->f3->set("SESSION.loginerror", "");
            $this->f3->reroute('/');
        }
        else{
            $this->f3->set("SESSION.loginerror", "Wrong password");
            $this->f3->reroute('/login');
        }
    }

    public function profile($f3){
        if($this->f3->get('SESSION.user')===null){
            $this->f3->reroute('/login');
            exit;
        }

        $users = new Users($this->db);
        $user = $users->getByUsername($f3->get('SESSION.user'));

        $f3->set('user', $user[0]); // {{@user}}
        $f3->set('header_title', 'Edit Profile');
        $f3->set('subtitle', 'Edit Profile');
        $f3->set('content', 'profile.html');
        echo $this->template->render('layout.html');
        $f3->clear('SESSION.editprofileerror');
        $f3->clear('SESSION.editprofilesuccess');
    }

    public function doUpdate($f3){
        $input = $f3->get('POST');

        $validate = true;
        $validate_txt = array();
        $validate_txt['username'] = array();
        $validate_txt['password'] = array();
        $validate_txt['fullname'] = array();
        $validate_txt['address'] = array();
        $validate_txt['email'] = array();
        $validate_txt['oldPassword'] = array();

        if($f3->get('POST.submit')!==""){
            $users = new Users($this->db);

            foreach ($input as $key => $value) {
                if($key !== "password"){
                    if ($value === "" || $value === null && $key !== 'submit') {
                        $validate = false;
                        array_push($validate_txt[$key], "Please dont leave it blank");
                    }
                }
            }

            if (!$this->audit->email($input['email'])) {
                $validate = false;
                array_push($validate_txt["email"], "Please input valid email address");
            }

            if (strlen($input['username']) < 4) {
                $validate = false;
                array_push($validate_txt['username'], "Username has to be atleast 4 characters");
            }

            if($input['password']!==""){
                if (strlen($input['password']) < 4) {
                    $validate = false;
                    array_push($validate_txt['password'], "Password has to be atleast 4 characters");
                }
            }

            for ($i = 0; $i < count($users->all()); $i++) {
                if ($users->all()[$i]->email === $input['email'] && $users->all()[$i]->id !== $users->getByUsername($f3->get('SESSION.user'))[0]->id) {
                    $validate = false;
                    array_push($validate_txt["email"], "Email exists");
                }
                if ($users->all()[$i]->username === $input['username'] && $users->all()[$i]->id !== $users->getByUsername($f3->get('SESSION.user'))[0]->id) {
                    $validate = false;
                    array_push($validate_txt["username"], "Username exists");
                }
            }

            if(!password_verify($input['oldPassword'], $users->getByUsername($f3->get('SESSION.user'))[0]->password)){
                $validate = false;
                array_push($validate_txt["oldPassword"], "Password does not match");
            }

            if($validate){
                if($input['password']===""){
                    $f3->set('POST.password', $users->getByUsername($f3->get('SESSION.user'))[0]->password);
                }
                else{
                    $f3->set('POST.password', password_hash($input['password'], PASSWORD_DEFAULT));
                }
                $users->edit($f3->get('SESSION.user'));
                $f3->set('SESSION.user', $input['username']);
                $f3->set('SESSION.editprofilesuccess', "Profile has been successfully edited");
            }
            else{
                $f3->set('SESSION.editprofileerror', $validate_txt);
            }

            $f3->reroute('/profile');
        }

    }

    public function logout(){
        $this->f3->clear('SESSION');
        $this->f3->reroute('/');
        exit;
    }
} 